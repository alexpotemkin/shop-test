from testshop.shop.models import ShoppingCart
from django.db.models import Sum

def get_cart_status(user, sessionkey):
    try:
        sc = ShoppingCart.objects.filter(user=user).filter(sessionkey=sessionkey).aggregate(amount=Sum('quantity'))
        if sc['amount'] is None:
            amount = 0
        else:
            amount = sc['amount']
        total = 0
        for item in ShoppingCart.objects.filter(user=user).filter(sessionkey=sessionkey):
            total = total + (item.quantity * item.product.price)
        return amount, total
    except ShoppingCart.DoesNotExist:
        return 0, 0


def update_product_in_cart(scid, value):
    try:
        sgUpdated = ShoppingCart.objects.get(id=scid)
        if sgUpdated != None:
            sgUpdated.quantity = int(value)
            sgUpdated.save()
            return True
        else:
            return False
    except ShoppingCart.DoesNotExist:
        return False


def delete_product_from_cart(user, sessionkey, shoppingcart_item):
    try:
        ShoppingCart.objects.filter(user=user).filter(sessionkey=sessionkey).filter(id=shoppingcart_item).delete()
        return True
    except ShoppingCart.DoesNotExist:
        return False
