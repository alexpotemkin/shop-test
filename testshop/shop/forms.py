from testshop.shop.models import Feedback
from django import forms


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['email', 'subject', 'text']


    def do_save(self, user):
        data = self.cleaned_data
        feedback = Feedback.objects.create(user=user, email=data['email'], subject=data['subject'], text=data['text'])
        feedback.save()

