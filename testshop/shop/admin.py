from django.contrib import admin
from testshop.shop.models import Category, Product, ShoppingCart, Feedback
from django.forms import TextInput
from django.db import models
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


class LinkedInline(admin.options.InlineModelAdmin):
    template = "admin/linked.html"
    admin_model_path = None

    def __init__(self, *args):
        super(LinkedInline, self).__init__(*args)
        if self.admin_model_path is None:
            self.admin_model_path = self.model.__name__.lower()


class ProductsInline(LinkedInline):
    model = Product
    fields = ('id', 'article', 'photo_', 'name', 'price')
    readonly_fields = ('id', 'article', 'photo_', 'name', 'price')
    extra = 0
    can_delete = False
    max_num = 0


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','name','slug')
    fields = ['name', 'slug']
    list_display_links = ('name',)
    ordering = ('id',)
    verbose_name_plural = 'Categories'
    inlines = [ProductsInline, ]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'100'})},
    }
admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id','category', 'article', 'photo_', 'name', 'price',)
    list_display_links = ('article','name',)
    list_filter = ('category',)
    search_fields = ('article','name',)
    ordering = ('id','category',)
    verbose_name_plural = 'Products'
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'100'})},
        models.IntegerField: {'widget': TextInput(attrs={'size':'50'})},
    }

    def response_change(self, request, obj):
        if request.POST.has_key("_save"):
            idcategory = request.GET.get('idcategory')
            if not idcategory is None:
                return HttpResponseRedirect(reverse("admin:index") + self.opts.app_label + '/' + obj.category.__class__.__name__.lower() + '/' + idcategory + '/')
        return super(ProductAdmin, self).response_change(request, obj)

    def response_post_save_add(self, request, obj):
        if request.POST.has_key("_save"):
            idcategory = request.GET.get('idcategory')
            if not idcategory is None:
                return HttpResponseRedirect(reverse("admin:index") + self.opts.app_label + '/' + obj.category.__class__.__name__.lower() + '/' + idcategory + '/')
        return super(ProductAdmin, self).response_post_save_add(request, obj)

admin.site.register(Product, ProductAdmin)


class ShoppingCartAdmin(admin.ModelAdmin):
    list_display = ('user', 'sessionkey', 'product', 'quantity', 'total', )
    list_filter = ('user', 'sessionkey', )
    search_fields = ('user', 'sessionkey', )
    ordering = ('user', 'sessionkey', )
    verbose_name_plural = 'Shopping carts'
admin.site.register(ShoppingCart, ShoppingCartAdmin)


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('id', 'datefeedback', 'user', 'email', 'subject', 'answered', )
    list_display_links = ('datefeedback', 'user', 'email', )
    list_filter = ('datefeedback', )
    search_fields = ('user', 'email', )
    ordering = ('id', 'datefeedback', 'email', )
    verbose_name_plural = 'Feedback'
admin.site.register(Feedback, FeedbackAdmin)