from testshop.shop.models import Category, Product, ShoppingCart
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from testshop.shop.cart import update_product_in_cart, delete_product_from_cart, get_cart_status
from testshop.shop.utils import template_exists
from testshop.shop.forms import FeedbackForm
from django import forms



def web_response(request, template_name, **params):
    if not request.session.get('has_session'):
        request.session['has_session'] = True
    return render_to_response(template_name, params, context_instance=RequestContext(request))


def web_response_redirect(request, to, *args, **kwargs):
    return redirect(to, *args, **kwargs)


def index(request):
    categoryList = Category.objects.all()
    productList = Product.objects.all()
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user,request.session.session_key)
    categoryName = ''
    return web_response(request,
                        'catalog.html',
                        categoryList=categoryList,
                        productList=productList,
                        total=total,
                        amount=amount,
                        categoryName=categoryName,
                        menuItem='catalog'
    )


def view_cart(request):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    scList = ShoppingCart.objects.filter(user=user).filter(sessionkey=request.session.session_key)
    amount, total = get_cart_status(user,request.session.session_key)
    return web_response(request, 'cart.html', scList=scList, amount=amount, total=total, menuItem='catalog')


def view_category(request, category=None):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user,request.session.session_key)
    try:
        currentCategory = Category.objects.get(slug=category)
        categoryName = currentCategory.name
        categoryList = Category.objects.all()
        productList = Product.objects.filter(category=currentCategory.id)
        return web_response(request, 'catalog.html', categoryList=categoryList, productList=productList, total=total, amount=amount, categoryName=categoryName, menuItem='catalog')
    except Category.DoesNotExist:
        errorMessage = 'Category "' + category + '" does not exist.'
        return web_response(request,'404.html', errorMessage=errorMessage, total=total, amount=amount, menuItem='catalog')


def add_to_cart(request, product=None):
    if not request.session.get('has_session'):
        request.session['has_session'] = True
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    try:
        currentProduct = Product.objects.get(id=product)
        checkProduct = ShoppingCart.objects.filter(user=user).filter(sessionkey=request.session.session_key).filter(product=product).count()
        if checkProduct == 0:
            addedProduct = ShoppingCart(user=user, sessionkey=request.session.session_key, product=currentProduct, quantity=1)
            addedProduct.save()
        else:
            currentSc = ShoppingCart.objects.filter(user=user).filter(sessionkey=request.session.session_key).get(product=product)
            amount = currentSc.amount + 1
            currentSc.amount = amount
            currentSc.save()
        return web_response_redirect(request, 'view_category', category=currentProduct.category_slug())
    except Product.DoesNotExist:
        amount, total = get_cart_status(user,request.session.session_key)
        errorMessage = 'Product with id=' + product + ' does not exist.'
        return web_response(request,'404.html', errorMessage=errorMessage, total=total, amount=amount, menuItem='catalog')
    except ShoppingCart.DoesNotExist:
        return web_response_redirect(request, 'index')



def del_from_cart(request, shoppingcart_item=None):
    if not request.session.get('has_session'):
        request.session['has_session'] = True
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    if delete_product_from_cart(user, request.session.session_key, shoppingcart_item):
        return web_response_redirect(request, 'view_cart')



def update_cart(request):
    if not request.session.get('has_session'):
        request.session['has_session'] = True
    for key, value in request.POST.iteritems():
        if 'scid-' in key:
            sc_id = int(key.replace('scid-',''))
            if update_product_in_cart(sc_id, value):
                return web_response_redirect(request, 'view_cart')


def view_checkout(request):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user, request.session.session_key)
    return web_response(request, 'checkout.html', amount=amount, total=total, menuItem='catalog')


def feedback(request):
    form = FeedbackForm(request.POST or None)
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user,request.session.session_key)
    return web_response(request,'feedback.html', form=form, amount=amount, total=total, success=False, menuItem='feedback')


def feedback_save(request):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user,request.session.session_key)
    form = FeedbackForm(request.POST or None)
    try:
        if form.is_valid():
            form.do_save(user)
            form = FeedbackForm()
            return web_response(request, 'feedback.html', amount=amount, total=total, success=True, form=form, menuItem='feedback')
        else:
            return web_response(request, 'feedback.html', amount=amount, total=total, success=False, form=form, menuItem='feedback')
    except forms.ValidationError:
        return web_response(request, 'feedback.html', amount=amount, total=total, success=False, form=form, menuItem='feedback')


def view_static_page(request, page=None):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = None
    amount, total = get_cart_status(user,request.session.session_key)
    if not page is None and template_exists(page):
        return web_response(request, page + '.html', amount=amount, total=total, menuItem=page)
    else:
        errorMessage = 'Page with name=' + page + ' does not exist.'
        return web_response(request,'404.html', errorMessage=errorMessage, total=total, amount=amount, menuItem='catalog')