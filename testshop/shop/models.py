from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.utils.text import slugify


class Category(models.Model):
    name = models.CharField(verbose_name='category name', max_length=255)
    slug = models.CharField(verbose_name='category url', max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'categories'
        ordering = ['id','name',]

    def __unicode__(self):
        return str(self.id) + ' - ' + self.name

    def save(self, *args, **kwargs):
        if len(self.slug) == 0:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

class Product(models.Model):
    category = models.ForeignKey(Category)
    article = models.IntegerField(verbose_name='article', default=0)
    name = models.CharField(verbose_name='product name', max_length=255)
    slug = models.CharField(verbose_name='product url', max_length=255, blank=True, null=True)
    desc = models.TextField(verbose_name='description')
    photo = models.ImageField(verbose_name='image', upload_to='product/')
    price = models.DecimalField(verbose_name='price', max_digits=15, decimal_places=2, default=0)

    class Meta:
        verbose_name_plural = 'Products'
        ordering = ['id','article','name',]

    def __unicode__(self):
        return str(self.id) + ' - ' +  str(self.article) + ' - ' + self.name

    def photo_(self):
        return u'<img width="50" height="50" src="%s" />' % self.photo.url
    photo_.short_description = 'Image'
    photo_.allow_tags = True

    def category_slug(self):
        return self.category.slug

    def category_name(self):
        return self.category.name

    def save(self, *args, **kwargs):
        if len(self.slug) == 0:
            self.slug = slugify(self.name)
        super(Product, self).save(*args, **kwargs)


class ShoppingCart(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    sessionkey = models.CharField(max_length=100)
    product = models.ForeignKey(Product)
    quantity = models.IntegerField()

    class Meta:
        ordering = ['id','user','sessionkey',]

    def __unicode__(self):
        if self.user is None:
            return str(self.id) + ' - ' + self.sessionkey + ' - None'
        else:
            return str(self.id) + ' - ' + self.sessionkey + ' - ' + self.user.username

    def photo(self):
        return self.product.photo

    def name(self):
        return self.product.name

    def article(self):
        return self.product.article

    def price(self):
        return self.product.price

    def total(self):
        return self.product.price * self.quantity


class Feedback(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    email = models.EmailField(verbose_name='e-mail', max_length=100, blank=False, null=False)
    subject = models.CharField(verbose_name='subject', max_length=255, blank=False, null=False)
    text = models.TextField(verbose_name='text', blank=False, null=False)
    answered = models.BooleanField(verbose_name='answered', default=False)
    datefeedback = models.DateField(verbose_name='date', default=datetime.today())

    class Meta:
        ordering = ['id','user','email','datefeedback',]

    def __unicode__(self):
        if self.user is None:
            return str(self.id) + ' - ' + self.email + ' - ' + str(self.datefeedback) + ' - ' + self.subject
        else:
            return str(self.id) + ' - ' + self.user.email + ' - ' + str(self.datefeedback) + ' - ' + self.subject



