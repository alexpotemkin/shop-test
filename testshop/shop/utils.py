from django.template import loader, TemplateDoesNotExist

def template_exists(template_name):
    try:
        loader.get_template(template_name + '.html')
        return True
    except TemplateDoesNotExist:
        return False
