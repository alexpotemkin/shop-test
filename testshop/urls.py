from django.conf.urls import patterns, url, include
from django.conf import settings


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls), name='admin'),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)

urlpatterns += patterns('testshop.shop.views',
                        url(r'^$', 'index', name='index'),
                        url(r'^category/(?P<category>[\w\W]+)/$', 'view_category', name='view_category'),
                        url(r'^checkout/$', 'view_checkout', name='view_checkout'),
                        url(r'^feedback/$', 'feedback', name='feedback'),
                        url(r'^feedback/send/$', 'feedback_save', name='feedback_save'),
                        url(r'^cart/$', 'view_cart', name='view_cart'),
                        url(r'^cart/add_to_cart/(?P<product>\d+)/$', 'add_to_cart', name='add_to_cart'),
                        url(r'^cart/del_from_cart/(?P<shoppingcart_item>\d+)/$', 'del_from_cart', name='del_from_cart'),
                        url(r'^cart/update_cart/$', 'update_cart', name='update_cart'),
                        url(r'^pages/(?P<page>[\w\W]+)/$', 'view_static_page', name='view_static_page'),
)

